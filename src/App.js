import React from 'react';
import {
  StylesProvider,
  createMuiTheme,
  ThemeProvider,
  CssBaseline,
  makeStyles,
} from '@material-ui/core';
import { AppContextProvider } from './Contexts/app.store';
import { default as ContentHeader } from './Layout/Header';
import HomePage from './Pages/HomePage';
import Layout, {
  getContent,
  getInsetContainer,
  getInsetSidebar,
  getHeader,
  Root,
} from '@mui-treasury/layout';
import styled from 'styled-components';
import Recommend from './Pages/Recommend';
const theme = createMuiTheme({
  typography: {
    fontFamily: ['Kanit', 'sans-serif'].join(','),
  },
  breakpoints: {},
});
const InsetContainer = getInsetContainer(styled);
const InsetSidebar = getInsetSidebar(styled);
const Content = getContent(styled);
const Header = getHeader(styled);
const scheme = Layout();
scheme.configureHeader((builder) => {
  builder.registerConfig('xs', {
    position: 'fixed',
    initialHeight: 54,
  });
});
scheme.configureInsetSidebar((builder) => {
  builder
    .create('insetSidebar', { anchor: 'right' })
    .registerFixedConfig('xs', {
      width: 0,
    })
    .registerFixedConfig('md', {
      width: 300,
    });
});
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#fafafa',
  },
}));
function App() {
  const classes = useStyles();
  return (
    <Root scheme={scheme}>
      <StylesProvider injectfirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <AppContextProvider>
            <Header>
              <ContentHeader />
            </Header>
            <Content className={classes.root}>
              <InsetContainer
                maxWidth={'md'}
                disableGutters
                rightSidebar={
                  <InsetSidebar
                    classes={{
                      paper: classes.root,
                    }}
                    sidebarId={'insetSidebar'}
                  >
                    <Recommend />
                  </InsetSidebar>
                }
              >
                <HomePage />
              </InsetContainer>
            </Content>
          </AppContextProvider>
        </ThemeProvider>
      </StylesProvider>
    </Root>
  );
}

export default App;
