import React, { createContext } from 'react';
export const AppContext = createContext({});
export const AppContextProvider = ({ children }) => {
  const store = {};
  return <AppContext.Provider value={store}>{children}</AppContext.Provider>;
};
