import React from 'react';
import { makeStyles, Box, Container } from '@material-ui/core';
import HomeIG from '../Components/Icons/HomeIG';
import DirectIG from '../Components/Icons/DirectIG';
import NavigatorIG from '../Components/Icons/NavigatorIG';
import HeartIG from '../Components/Icons/HeartIG';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    border: `solid 1px ${theme.palette.divider}`,
    backgroundColor: 'white',
    minHeight: 54,
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(1),
  },
  logo: {
    cursor: 'pointer',
    maxHeight: '29px',
    maxWidth: '100%',
    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
    },
    [theme.breakpoints.up('md')]: {
      marginLeft: '-20px',
    },
  },
  avatar: {
    borderRadius: '50%',
    marginLeft: '22px',
    width: '22px',
    height: '23px',
    marginBottom: '5px',
    marginRight: '-5px',

    [theme.breakpoints.down('xs')]: {
      marginRight: '0',
    },
    [theme.breakpoints.down('md')]: {
      marginRight: '10px',
    },
  },
  icon: {
    marginLeft: '22px',
    width: '22px',
    height: '23px',
    marginBottom: '5px',
    cursor: 'pointer',
  },
  spring: {
    flex: 1,
  },
  container: {
    display: 'flex',
  },
  search: {
    width: '215px',
    height: '28px',
    marginBottom: '5px',
    borderColor: theme.palette.divider,
    background: '#fafafa',
    borderWidth: '1px',
    borderRadius: '3px',
    cursor: 'text',
    fontSize: '14px',
    fontWeight: 300,
    textAlign: 'center',
    marginLeft: '134px',
    marginRight: '22px',
    '&:focus': {
      textAlign: 'left',
    },
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
}));
export default function Header() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {/* <Box
        position="fixed"
        className={classes.appBar}
        display="flex"
        alignItems="center"
        pt={1}
      > */}
      <Container className={classes.container} maxWidth="md">
        <img
          src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png"
          className={classes.logo}
          alt="logo"
        />
        <div className={classes.spring}></div>
        <input
          autoCapitalize="none"
          placeholder="Search"
          className={classes.search}
        />
        <div className={classes.spring}></div>
        <HomeIG className={classes.icon} />
        <DirectIG className={classes.icon} />
        <NavigatorIG className={classes.icon} />
        <HeartIG className={classes.icon} />
        <img
          alt="avatar"
          className={classes.avatar}
          src="https://instagram.fbkk9-2.fna.fbcdn.net/v/t51.2885-19/s150x150/82703277_609998049824703_5867918683552088064_n.jpg?_nc_ht=instagram.fbkk9-2.fna.fbcdn.net&_nc_ohc=Xjhqv8Kt8BMAX-5_DZn&oh=3494ac763fa52e1bb2a7cc66d34d12c3&oe=5F8E0725"
        />
      </Container>
      {/* </Box> */}
    </div>
  );
}
