import {
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  Link,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  makeStyles,
  Typography,
} from '@material-ui/core';
import React from 'react';
const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: 30,
    height: '100%',
    minWidth: 300,
    marginTop: 25,
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  avatar: {
    width: 56,
    height: 56,
  },
  name: {
    fontWeight: 600,
  },
  follow: {
    fontSize: 12,
    textAlign: 'right',
    color: '#3faef7',
    fontWeight: 'bold',
  },
  followBy: {
    fontSize: 12,
  },
  avatarRecommend: {
    width: 32,
    height: 32,
  },
  spring: {
    flex: 1,
  },
}));
export default function Recommend() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.container}>
        <Box display="flex" alignItems="center">
          <Box>
            <Avatar className={classes.avatar} src="profile.jpeg" />
          </Box>
          <Box pl="16px">
            <Link
              underline="none"
              className={classes.name}
              href="#"
              color="textPrimary"
            >
              koravit
            </Link>
          </Box>
        </Box>
        <Box pt={2} display="flex">
          <Box flexGrow={1}>
            <Typography color="textSecondary" variant="subtitle2">
              Suggestions For You
            </Typography>
          </Box>
          <Box>
            <Typography
              style={{ fontWeight: 'bold' }}
              color="textPrimary"
              variant="caption"
            >
              <Link href="#" underline="none" color="inherit">
                See All
              </Link>
            </Typography>
          </Box>
        </Box>
        <Box>
          <List disablePadding={true} dense={true}>
            {[...Array(5).keys()].map(() => (
              <ListItem disableGutters>
                <ListItemAvatar>
                  <Avatar
                    className={classes.avatarRecommend}
                    src="profile.jpeg"
                  ></Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary="koravit"
                  secondaryTypographyProps={{ className: classes.followBy }}
                  primaryTypographyProps={{ className: classes.name }}
                  secondary="ติดตามโดย koravit1"
                />
                <div className={classes.spring}></div>
                <Link
                  className={classes.follow}
                  display="inline"
                  href="#"
                  underline="none"
                >
                  Follow
                </Link>
              </ListItem>
            ))}
          </List>
        </Box>
      </div>
    </>
  );
}
