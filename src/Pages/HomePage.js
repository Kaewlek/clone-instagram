import {
  Avatar,
  Box,
  Card,
  CardHeader,
  Container,
  Divider,
  Link,
  makeStyles,
  Paper,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import React from 'react';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import DirectIG from '../Components/Icons/DirectIG';
import HeartIG from '../Components/Icons/HeartIG';
import CommentIG from '../Components/Icons/CommentIG';
import FavoriteIG from '../Components/Icons/FavoriteIG';
const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down('xs')]: {},
  },
  storyWrapper: {
    [theme.breakpoints.down('xs')]: {
      minWidth: 480,
      marginTop: 1,
    },
    [theme.breakpoints.up('sm')]: {
      maxWidth: 614,
      marginTop: '30px',
    },
    [theme.breakpoints.up('md')]: {
      maxWidth: 614,
      marginTop: '30px',
      marginLeft: '-20px',
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: 614,
      marginTop: '30px',
      marginLeft: '20px',
    },
  },
  story: {
    padding: '16px 8px',
    border: `solid 1px ${theme.palette.divider}`,
    overflowX: 'hidden',
    display: 'flex',
  },
  content: {
    marginTop: '30px',
    border: `solid 1px ${theme.palette.divider}`,
    [theme.breakpoints.down('xs')]: {
      background: 'inherit',
      border: 'none',
    },
  },
  cardAvatar: {
    padding: '8px',
  },
  cardTitle: { fontWeight: 600, fontSize: 14 },
  cardSubTitle: { fontSize: 12 },
  cardImage: {
    width: '100%',
  },
  dot: {
    width: 6,
    height: 6,
    backgroundColor: '#a8a8a8',
    borderRadius: '50%',
  },
  dotActive: {
    width: 6,
    height: 6,
    backgroundColor: '#0095f6',
    borderRadius: '50%',
  },
  avatarComment: {
    width: 20,
    height: 20,
  },
}));
export default function HomePage() {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <>
      <Container
        disableGutters
        className={classes.root}
        maxWidth={useMediaQuery(theme.breakpoints.down('md')) ? 'sm' : 'md'}
      >
        <div className={classes.storyWrapper}>
          <Paper elevation={0} className={classes.story}>
            <StoryAvatar name="koravit" dataSrc={[...Array(20).keys()]} />
          </Paper>
          {[...Array(10).keys()].map(() => (
            <Card elevation={0} className={classes.content}>
              <Box p="8px" alignItems="center" display="flex">
                <Box>
                  <StoryAvatar size="small" dataSrc={[1]} />
                </Box>
                <Box>
                  <Typography className={classes.cardTitle}>koravit</Typography>
                  <Typography className={classes.cardSubTitle}>
                    location
                  </Typography>
                </Box>
                <Box flexGrow={1}></Box>
                <Box pt="5px">
                  <MoreHorizIcon fontSize="small" />
                </Box>
              </Box>
              <img className={classes.cardImage} src="profile.jpeg" />
              <Box position="relative">
                <Box
                  display="flex"
                  flexDirection="row"
                  alignItems="center"
                  alignContent="center"
                  position="absolute"
                  left="0"
                  right="0"
                  bottom="0"
                  top="17px"
                  justifyContent="center"
                >
                  <Box mr="4px">
                    <div className={classes.dotActive}></div>
                  </Box>
                  <Box mr="4px">
                    <div className={classes.dot}></div>
                  </Box>
                  <Box mr="4px">
                    <div className={classes.dot}></div>
                  </Box>
                  <Box mr="4px">
                    <div className={classes.dot}></div>
                  </Box>
                  <Box mr="4px">
                    <div className={classes.dot}></div>
                  </Box>
                </Box>
              </Box>
              <Box py="8px" px="16px" display="flex" alignItems="center">
                <Box>
                  <HeartIG />
                </Box>
                <Box ml="16px">
                  <CommentIG />
                </Box>
                <Box ml="16px">
                  <DirectIG />
                </Box>
                <Box flexGrow={1}></Box>
                <Box ml="16px">
                  <FavoriteIG />
                </Box>
              </Box>
              <Box display="flex" mb="8px" px="16px">
                <Avatar
                  className={classes.avatarComment}
                  src="profile.jpeg"
                ></Avatar>
                <Box mx="4px" fontSize="14">
                  Liked by
                </Box>
                <Box fontWeight="bold" fontSize="12">
                  koravit
                </Box>
                <Box mx="4px" fontSize="14">
                  and
                </Box>
                <Box fontWeight="bold" fontSize="12">
                  999,999 others
                </Box>
              </Box>
              <Box mb="4px" display="flex" alignItems="center" px="16px">
                <Box mr="4px" fontWeight="bold" fontSize="12">
                  koravit2
                </Box>
                <Box fontSize="12">หล่อจังเลยครับ</Box>
                <Box flexGrow={1}></Box>
                <HeartIG style={{ width: 12, height: 12, cursor: 'pointer' }} />
              </Box>
              <Box mb="4px" display="flex" px="16px">
                <Box mr="4px" fontWeight="bold" fontSize="12">
                  koravit3
                </Box>
                <Box fontSize="12">หล่อจังเลยครับ2</Box>
                <Box flexGrow={1}></Box>
                <HeartIG style={{ width: 12, height: 12, cursor: 'pointer' }} />
              </Box>
              <Box mb="4px" px="16px" fontSize="10px" color="#bfbfbf">
                8 HOURS AGO
              </Box>
              <Divider variant="fullWidth" />
              <Box display="flex" alignItems="center" py="12px" px="16px">
                <Box>
                  <TextField
                    variant="standard"
                    fullWidth
                    placeholder="Add a comment..."
                    InputProps={{
                      disableUnderline: true,
                    }}
                  />
                </Box>
                <Box flexGrow={1}></Box>
                <Box>
                  <Link
                    style={{ color: '#3faef7', fontWeight: 'bold' }}
                    underline="none"
                    href="#"
                  >
                    Post
                  </Link>
                </Box>
              </Box>
            </Card>
          ))}
        </div>
      </Container>
    </>
  );
}

const storyStyles = makeStyles((theme) => ({
  avatarWrapper: {
    padding: '0 8px',
    cursor: 'pointer',
  },
  avatar: {
    borderRadius: 'inherit',
    padding: 2,
    backgroundColor: 'white',
    objectFit: 'cover',
    height: 'auto',
    width: '100%',
  },
  isRead: (props) => ({
    border: 'solid 3px transpent',
    display: 'flex',
    padding: 2,
    width: props.size === 'small' ? 42 : 64,
    height: props.size === 'small' ? 42 : 64,
    borderRadius: '50%',
    backgroundImage:
      'linear-gradient(45deg, #fab271, #ee7367, #e78aba, #c13584, #e1306c)',
  }),
  storyUsername: {
    fontSize: 12,
    textAlign: 'center',
    marginTop: 2,
    color: '#555',
  },
}));
const StoryAvatar = ({
  dataSrc = [],
  size = 'normal',
  name = '',
  className = {},
}) => {
  const classes = storyStyles({ size });
  return (
    <>
      {dataSrc &&
        dataSrc.map((item, index) => (
          <div className={className}>
            <div className={classes.avatarWrapper}>
              <div className={classes.isRead}>
                <img
                  alt="story"
                  className={classes.avatar}
                  src="profile.jpeg"
                />
              </div>
              {name && (
                <Typography className={classes.storyUsername}>
                  {`${name} ${index + 1}`}
                </Typography>
              )}
            </div>
          </div>
        ))}
    </>
  );
};
